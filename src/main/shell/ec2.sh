#!/bin/bash

SOURCE="$0"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

EC2_HOME="$( cd -P "$( dirname "$SOURCE" )" && cd .. && pwd)"
EC2_PROFILE="${HOME}/.ec2"
EC2_CONFIG="${EC2_PROFILE}/config"
EC2_LIBS="${EC2_HOME}/lib/*"

if [[ -f "${EC2_CONFIG}" ]]; then
    source "${EC2_CONFIG}"
else
    echo "Configuration file doesn't exist: ${EC2_CONFIG}"
    echo "Create file and set following variables:"
    echo "  AWS_ACCESS_KEY  - Amazon AWS API access key"
    echo "  AWS_SECREY_KEY  - Amazon AWS API secret key"
    echo "  AWS_PRIVATE_KEY - private key to be used for SSH/SCP"
    exit 1
fi

if [[ "${AWS_ACCESS_KEY}" == "" ]]; then
    echo "AWS_ACCESS_KEY not set"
    exit 1
fi

if [[ "${AWS_SECRET_KEY}" == "" ]]; then
    echo "AWS_SECRET_KEY not set"
    exit 1
fi

if [[ ! -f "${AWS_PRIVATE_KEY}" ]]; then
    echo "Private key not found: ${AWS_PRIVATE_KEY}"
    exit 1
fi

COMMAND=$1

case ${COMMAND} in
	"list")
		java -cp "${EC2_LIBS}" ru.nikisoft.ec2.ListInstances "${AWS_ACCESS_KEY}" "${AWS_SECRET_KEY}"
		;;
	"start"|"stop"|"status")
	    INSTANCE=$2

	    if [[ ${INSTANCE} == "" ]]; then
            echo "Instance name not specified"
		    exit 1
        fi

        case ${COMMAND} in
            "start")
		        java -cp "${EC2_LIBS}" ru.nikisoft.ec2.StartInstance "${AWS_ACCESS_KEY}" "${AWS_SECRET_KEY}" "${INSTANCE}"
                ;;
            "stop")
                java -cp "${EC2_LIBS}" ru.nikisoft.ec2.StopInstance "${AWS_ACCESS_KEY}" "${AWS_SECRET_KEY}" "${INSTANCE}"
                ;;
            "status")
                java -cp "${EC2_LIBS}" ru.nikisoft.ec2.StatusInstance "${AWS_ACCESS_KEY}" "${AWS_SECRET_KEY}" "${INSTANCE}"
                ;;
        esac
		;;
	"ssh"|"scp"|"telnet")
	    INSTANCE=$2

	    if [[ ${INSTANCE} == "" ]]; then
            echo "Instance name not specified"
		    exit 1
        fi

		INSTANCE_ID=`cat "${EC2_PROFILE}/instances" | grep -i "$2" | awk -F = '{print $1}'`

        if [[ "${INSTANCE_ID}" == "" ]]; then
            echo "Instance not found: ${INSTANCE}"
            exit 0
        fi

        INSTANCE_STATE=`cat "${EC2_PROFILE}/instances" | grep -i "${INSTANCE_ID}.state" | awk -F = '{print $2}'`

        if [[ "${INSTANCE_STATE}" != "running" ]]; then
            echo "Instance doesn't seem to be running: ${INSTANCE} (${INSTANCE_ID})"
            exit 0
        fi

        INSTANCE_IP=`cat "${EC2_PROFILE}/instances" | grep -i "${INSTANCE_ID}.ip" | awk -F = '{print $2}'`

        if [[ "${INSTANCE_IP}" == "" ]]; then
            echo "No IP address associated with the instance: ${INSTANCE} (${INSTANCE})"
            exit 0
        fi

        case ${COMMAND} in
            "scp")
                if [[ ! -f $3 ]]; then
                    echo "File not found: $3"
                    exit 0
                fi

                scp -i "${AWS_PRIVATE_KEY}" $3 "ec2-user@${INSTANCE_IP}:~"
                ;;
            "ssh")
		        ssh -i "${AWS_PRIVATE_KEY}" "ec2-user@${INSTANCE_IP}"
		        ;;
		    "telnet")
		        shift 2
		        telnet "${INSTANCE_IP}" $@
		        ;;
		esac
		;;
	*)
	    echo "Usage: `basename $0` (list|start|stop|status|ssh|scp) [instance name]"
	    ;;
esac
