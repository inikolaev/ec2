#!/bin/bash

_foo() 
{
	local cur prev opts
	COMPREPLY=()
	cur="${COMP_WORDS[COMP_CWORD]}"
	prev="${COMP_WORDS[COMP_CWORD-1]}"
	opts="list start stop status ssh scp telnet"

    if [[ ${COMP_CWORD} == 3 ]]; then
        return 0
    fi

	if [ "${prev}" == "start" -o "${prev}" == "stop" -o "${prev}" == "status" -o "${prev}" == "ssh" -o "${prev}" == "scp" -o "${prev}" == "telnet" ]; then
		local IFS=$'\n'
		cur=${cur//\\ / }
		shopt -s nocasematch
			
		local names=$(cat ~/.ec2/instances | grep -v "^#" | grep -v ".ip *=" | grep -v ".state *=" | awk -F = '{print $2}')
		        			                
		local i=0
		local name
			    
		for name in ${names}; do
			if [[ "${name}" == "${cur}"* ]]; then
				COMPREPLY[$i]=${name}
				(( i++ ))    
			fi
		done
		    
		shopt -u nocasematch
		return 0
	fi

	COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
	return 0
}
complete -o filenames -F _foo ec2
