package ru.nikisoft.ec2;

import com.amazonaws.services.ec2.model.Instance;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: inikolaev
 * Date: 7/13/13
 * Time: 6:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class StatusInstance extends BaseCommand {
    private final String name;

    public StatusInstance(String... args) throws IOException {
        super(args);

        Validate.isEqual(args.length, 3, "Wrong number of arguments: expected 3 was " + args.length);
        Validate.isNotBlank(args[2], "Instance name not specified");

        this.name = args[2];
    }

    @Override
    public void invoke() {
        Instance instance = null;
        String instanceId = getInstanceIdByName(name);

        if (instanceId == null) {
            System.out.println("Instance metadata not found in cache, trying to get it from AWS");

            instance = client.getInstanceByName(name);
            instanceId = instance.getInstanceId();
        } else {
            instance = client.getInstance(instanceId);
        }

        if (instance != null) {
            config.setProperty(instanceId, name);
            config.setProperty(instanceId + ".state", instance.getState().getName());

            if (instance.getPublicIpAddress() != null)
                config.setProperty(instanceId + ".ip", instance.getPublicIpAddress());

            System.out.println("Status: " + instance.getState().getName());
        } else {
            System.out.println("Instance \"" + name + "\" not found");
        }

        saveConfig();
    }

    public static void main(String[] args) throws IOException {
        StatusInstance command = new StatusInstance(args);
        command.invoke();
    }
}
