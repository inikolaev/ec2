package ru.nikisoft.ec2;

import com.amazonaws.services.ec2.model.Instance;

import java.io.*;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: inikolaev
 * Date: 7/13/13
 * Time: 7:42 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class BaseCommand {
    protected final AmazonWebServicesClient client;
    protected final Properties config;

    public BaseCommand(String...args) throws IOException {
        Validate.isNotLess(args.length, 2, "Wrong number of parameters: expected at least 2, was " + args.length);
        Validate.isNotBlank(args[0], "Parameter `accessKey` cannot be null or empty");
        Validate.isNotBlank(args[1], "Parameter `secretKey` cannot be null or empty");

        String accessKey = args[0];
        String secretKey = args[1];

        client = new AmazonWebServicesClient(accessKey.trim(), secretKey.trim());
        config = loadConfig();
    }

    protected Properties loadConfig() throws IOException {
        Properties properties = new Properties();

        String configPath = System.getProperty("user.home") + "/.ec2/instances";
        InputStream is = new FileInputStream(new File(configPath));
        properties.load(is);
        is.close();

        return properties;
    }

    protected void saveConfig() {
        try {
            String userHome = System.getProperty("user.home");
            File folder = new File(userHome + "/.ec2");
            folder.mkdirs();
            OutputStream os = new FileOutputStream(new File(folder, "instances"));
            config.store(os, "Instances");
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String getInstanceIdByName(String name) {
        for (Object key: config.keySet()) {
            if (name.equalsIgnoreCase(config.getProperty((String) key)))
                return (String) key;
        }

        return null;
    }

    protected void waitForInstanceStatus(String instanceId, String status, int timeout) {
        for (int i = 0; i < timeout; i++) {
            System.out.print(".");
            Instance instance = client.getInstance(instanceId);

            if (status.equalsIgnoreCase(instance.getState().getName())) {
                config.setProperty(instanceId + ".status", status);

                if (instance.getPublicIpAddress() != null)
                    config.setProperty(instanceId + ".ip", instance.getPublicIpAddress());
                else
                    config.remove(instanceId + ".ip");
                return;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        throw new RuntimeException("Instance \"" + instanceId + "\" failed to reach \"" + status + "\" status");
    }

    public abstract void invoke();
}
