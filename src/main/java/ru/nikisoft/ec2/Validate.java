package ru.nikisoft.ec2;

/**
 * Created with IntelliJ IDEA.
 * User: inikolaev
 * Date: 7/26/13
 * Time: 10:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class Validate {
    public static void isEqual(int value, int what, String message) {
        if (value != what)
            throw new IllegalArgumentException(message);
    }

    public static void isNotLess(int value, int what, String message) {
        if (value < what)
            throw new IllegalArgumentException(message);
    }

    public static void isNotBlank(String value, String message) {
        if (value == null || value.trim().length() == 0)
            throw new IllegalArgumentException(message);
    }
}
