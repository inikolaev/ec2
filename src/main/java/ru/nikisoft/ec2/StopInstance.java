package ru.nikisoft.ec2;

import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceState;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: inikolaev
 * Date: 7/13/13
 * Time: 6:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class StopInstance extends BaseCommand {
    private final String name;

    public StopInstance(String... args) throws IOException {
        super(args);

        Validate.isEqual(args.length, 3, "Wrong number of arguments: expected 3 was " + args.length);
        Validate.isNotBlank(args[2], "Instance name not specified");

        this.name = args[2];
    }

    @Override
    public void invoke() {
        String instanceId = getInstanceIdByName(name);

        if (instanceId == null) {
            System.out.println("Instance metadata not found in cache, trying to get it from AWS");

            Instance instance = client.getInstanceByName(name);

            if (instance == null) {
                System.out.println("Instance \"" + name + "\" not found");
                return;
            }

            instanceId = instance.getInstanceId();
            config.setProperty(instanceId, name);
            config.setProperty(instanceId + ".state", instance.getState().getName());

            if (instance.getPublicIpAddress() != null)
                config.setProperty(instanceId + ".ip", instance.getPublicIpAddress());

            saveConfig();
        }

        InstanceState currentState = client.getInstanceState(instanceId);
        if ("stopping".equalsIgnoreCase(currentState.getName())) {
            System.out.println("Instance is already stopping");
            return;
        } else if ("stopped".equalsIgnoreCase(currentState.getName())) {
            System.out.println("Instance is already stopped");
            return;
        }

        System.out.println("Stopping instance \"" + name + " (" + instanceId + ")\"");

        client.stopInstance(instanceId);
        waitForInstanceStatus(instanceId, "stopped", 600);
        System.out.println("\nDone");

        saveConfig();
    }

    public static void main(String[] args) throws IOException {
        StopInstance command = new StopInstance(args);
        command.invoke();
    }
}
