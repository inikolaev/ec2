package ru.nikisoft.ec2;

import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Tag;

import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: inikolaev
 * Date: 7/13/13
 * Time: 6:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class ListInstances extends BaseCommand {
    public ListInstances(String...args) throws IOException {
        super(args);
    }

    @Override
    public void invoke() {
        System.out.println(String.format("%-20s %-12s %-10s %-15s", "Name", "Instance ID", "State", "Public IP"));
        System.out.println("============================================================");

        List<Instance> instances = client.getInstances();
        config.clear();

        for (Instance instance: instances) {
            String name = getName(instance);
            System.out.println(String.format("%-20s %-12s %-10s %-15s", name, instance.getInstanceId(), instance.getState().getName(), instance.getPublicIpAddress()));
            config.put(instance.getInstanceId(), name);
            config.put(instance.getInstanceId() + ".state", instance.getState().getName());

            if (instance.getPublicIpAddress() != null)
                config.put(instance.getInstanceId() + ".ip", instance.getPublicIpAddress());
        }

        saveConfig();
    }

    private String getName(Instance instance) {
        List<Tag> tags = instance.getTags();

        for (Tag tag: tags) {
            if ("name".equalsIgnoreCase(tag.getKey()))
                return tag.getValue();
        }

        return instance.getInstanceId();
    }

    public static void main(String[] args) throws IOException {
        ListInstances command = new ListInstances(args);
        command.invoke();
    }
}
