package ru.nikisoft.ec2;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.*;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: inikolaev
 * Date: 7/16/13
 * Time: 8:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class AmazonWebServicesClient {
    protected final AmazonEC2Client client;

    public AmazonWebServicesClient(String accessKey, String secretKey) throws IOException {
        client = new AmazonEC2Client(new BasicAWSCredentials(accessKey, secretKey));
    }

    public Instance getInstance(String instanceId) {
        DescribeInstancesRequest request = new DescribeInstancesRequest().withInstanceIds(instanceId);
        DescribeInstancesResult result = client.describeInstances(request);
        return result.getReservations().get(0).getInstances().get(0);
    }

    public Instance getInstanceByName(String name) {
        List<Instance> instances = getInstances();

        for (Instance instance: instances) {
            List<Tag> tags = instance.getTags();

            for (Tag tag: tags) {
                if ("name".equalsIgnoreCase(tag.getKey()) && name.equalsIgnoreCase(tag.getValue()))
                    return instance;
            }
        }

        return null;
    }

    public List<Instance> getInstances() {
        List<Instance> instances = new LinkedList<Instance>();

        DescribeInstancesResult result = client.describeInstances();
        List<Reservation> reservations = result.getReservations();

        for (Reservation reservation: reservations) {
            instances.addAll(reservation.getInstances());
        }

        return instances;
    }

    public InstanceState getInstanceState(String instanceId) {
        return getInstance(instanceId).getState();
    }

    public void startInstance(String instanceId) {
        StartInstancesRequest request = new StartInstancesRequest().withInstanceIds(instanceId);
        StartInstancesResult result = client.startInstances(request);

        if (result.getStartingInstances().size() != 1 || !instanceId.equalsIgnoreCase(result.getStartingInstances().get(0).getInstanceId()))
            throw new RuntimeException("Error starting instance \"" + instanceId + "\"");
    }

    public void stopInstance(String instanceId) {
        StopInstancesRequest request = new StopInstancesRequest().withInstanceIds(instanceId);
        StopInstancesResult result = client.stopInstances(request);

        if (result.getStoppingInstances().size() != 1 || !instanceId.equalsIgnoreCase(result.getStoppingInstances().get(0).getInstanceId()))
            throw new RuntimeException("Error stopping instance \"" + instanceId + "\"");
    }
}
